/*jshint globalstrict: true, strict: true, jquery: true, browser: true, esversion: 6*/
/*global PersonResource, saveAs, $, includeHTML, getUrlParameter*/
"use strict";

let painting =  $(document.createElement( "html" ));
let painting_head =  $(document.createElement( "head" ));
let painting_body = $(document.createElement( "body" ));
let painting_head_link = $(document.createElement( "link" ));
painting_head_link.attr("rel", "stylesheet");
painting_head_link.attr("type", "text/css");
painting_head_link.attr("href", "messages.css");

let painting_jq_link = $(document.createElement( "script" ));
painting_jq_link.attr("src", "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js");

painting_head.append(painting_head_link);
painting_head.append(painting_jq_link);
painting.append(painting_head);
painting.append(painting_body);

String.prototype.replaceArray = function(find, replace) {
    let replaceString = this;
    let regex;
    for (let i = 0; i < find.length; i++) {
        regex = new RegExp(find[i], "g");
        replaceString = replaceString.replace(regex, replace[i]);
    }
    return replaceString;
};

$(document).ready(function() {
    let input = getUrlParameter("input") + ".html";
    includeHTML(input);
    let personResources = new PersonResource();
    personResources.parseChatMessages($('._3-96._2pio._2lek._2lel'));

    personResources.createBlocksWithWords();
    let output = getUrlParameter("output") + ".html";
    saveAs(output, painting.html());
});