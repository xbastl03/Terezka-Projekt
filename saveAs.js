/*jshint globalstrict: true, strict: true, jquery: true, browser: true, esversion: 6*/
/*global streamSaver, console*/
"use strict";

function saveAs(fileName, payload) {
    const fileStream = streamSaver.createWriteStream(fileName, {
        size: 22, // (optional) Will show progress
        writableStrategy: undefined, // (optional)
        readableStrategy: undefined  // (optional)
    });
    let success = function () {
      console.log("success");
    };


    let error = function () {
        console.log("error");
    };
    new Response(payload).body
        .pipeTo(fileStream)
        .then(success, error);
}
