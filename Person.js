/*jshint globalstrict: true, strict: true, jquery: true, browser: true, esversion: 6*/
/*global CleaningRegex*/
"use strict";

class PersonChat {
    constructor(name, index, href = false, emoticon = true) {
        this.name = name;
        this.index = index;
        this.emoticon = emoticon;
        this.href = href;

        this.raw_text = '';

        this.words = [];
        this.messages = [];
        this.clean_messages = [];
        this.words_count = [];


    }

    addMessage(message) {
        let whiteMessage = message.replace(CleaningRegex.white_re(), ' ');
        let hrefMessage = whiteMessage.replace(CleaningRegex.href_re(), ' ');
        let emoticonMessage = hrefMessage.replace(CleaningRegex.emoticon_re(), ' ');
        let rawMessage = whiteMessage;
        if (!this.href && !this.emoticon) {
            rawMessage = emoticonMessage;
        } else if (!this.href) {
            rawMessage = hrefMessage;
        } else if (!this.emoticon) {
            rawMessage = whiteMessage.replace(CleaningRegex.emoticon_re(), ' ');
        }
        this.messages.push(rawMessage);

        message = emoticonMessage.replace(CleaningRegex.words_re(), ' ');
        message = message.replace(CleaningRegex.number_re(), ' ');

        this.clean_messages.push(message);

    }

    addToRaw(message) {
        this.raw_text += ' ' + message;
    }

    addAllMessagesToRaw() {
        let index;
        this.raw_text = '';
        for (index = 0; index < this.messages.length; ++index) {
            this.addToRaw(this.messages[index]);
        }
    }

    processWords() {
        this.clean_messages.forEach((clean_message) => {

            clean_message = clean_message.replace(CleaningRegex.multiple_white(), ' ');
            let words = clean_message.split(" ");
            words.forEach((element) => {
                if (element !== '' && element !== null) {
                    element = element.toLowerCase();
                    element = element.replaceArray(CleaningRegex.find_czech(), CleaningRegex.replace_czech());

                    this.words.push(element);
                    let found = this.words_count.some(function (el) {
                        if (el[0] === element) {
                            el[1]++;
                            return true;
                        }
                    });
                    if (!found) {
                        this.words_count.push([element, 1]);
                    }
                }
            });
        });
    }
}