/*jshint globalstrict: true, strict: true, jquery: true, browser: true, esversion: 6*/
"use strict";
class CleaningRegex{
    static white_re(){return new RegExp(/(\s+)|(Pro\s+zvuk\s+klikněte)|(Pro\s+video\s+klikněte:)/, "gi");}
    static emoticon_re (){return  new RegExp(/([\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2694-\u2697]|\uD83E[\uDD10-\uDD5D]|❤|☺|(:D)|(:-\()|(:\))|(:-\*)|(:-\/)|(:-\))|(:-O)|(:-p))|(;D)|(\^_\^)|(;-\))|(:-D)|(:'\(|(:F))/, "gi");}
    static words_re (){return  new RegExp(/\++|\?+|\.+|!+|,+|[(){}]|\s+|:|%|\*|"|['`~°@$^&_]/, "gi");}
    static href_re (){return  new RegExp(/((https?:\/\/)?www2?\.[a-zA-Z0-9-.]+\.[a-z]+[^ ]+)|((https?:\/\/)[a-zA-Z0-9-.]+\.[a-z]+[^ ]+)/, "gi");}
    static number_re (){return  new RegExp(/\d+/, "gi");}
    static multiple_white (){return  new RegExp(/\s+/, "gi");}
    static special_char (){return  new RegExp(/[^A-Za-z0-9]/, "gi");}
    static find_czech (){return  ['ě','š','č','ř','ž','ý','á','í','é','ú','ů','ň','ď','ť','ó'];}
    static replace_czech (){return  ['e','s','c','r','z','y','a','i','e','u','u','n','d','t','o'];}
}

