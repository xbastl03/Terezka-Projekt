/*jshint globalstrict: true, strict: true, jquery: true, browser: true, esversion: 6*/
/*global PersonChat, $, painting_body*/
"use strict";

class PersonResource {
    constructor() {
        this.persons = [];
        this.number_of_persons = 0;
    }

    parseChatMessages(chat_messages) {
        let thisa = this;
        chat_messages.each(function () {
            let who_text = $(this).text();
            let person = thisa.persons[who_text];
            let text_message = $(this).next().text();
            if (person === undefined) {
                person = new PersonChat(who_text, thisa.number_of_persons);
                thisa.persons[who_text] = person;
                thisa.number_of_persons++;
            }
            person.addMessage(text_message);
        });
    }

    createSimpleTwoBlock() {
        let thisa = this;


        Object.keys(this.persons).forEach(function (key) {

            let person = thisa.persons[key];
            person.addAllMessagesToRaw(person);
            let person_messages_div = thisa.createPersonDiv(person);

            person_messages_div.append(thisa.createTitle(person));
            person_messages_div.append(thisa.createSimpleBlock(person));

            painting_body.append(person_messages_div);

        });
    }

    createFirstPersonWithWords() {
        let thisa = this;

        let i = 0;
        Object.keys(this.persons).forEach(function (key) {
            if (i == 0) {


                let person = thisa.persons[key];
                person.addAllMessagesToRaw(person);
                let person_messages_div = thisa.createPersonDiv(person);

                person_messages_div.append(thisa.createTitle(person));
                person_messages_div.append(thisa.createWords(person));
                person_messages_div.append(thisa.createSimpleBlock(person));

                painting_body.append(person_messages_div);
            }
            i++;
        });
    }

    createBlocksWithWords() {
        let thisa = this;


        Object.keys(this.persons).forEach(function (key) {

            let person = thisa.persons[key];
            person.addAllMessagesToRaw(person);
            let person_messages_div = thisa.createPersonDiv(person);

            person_messages_div.append(thisa.createTitle(person));
            person_messages_div.append(thisa.createWords(person));
            person_messages_div.append(thisa.createSimpleBlock(person));

            painting_body.append(person_messages_div);

        });
    }

    createWords(person) {
        person.processWords();

        let person_words_div = $(document.createElement("div"))
            .addClass('words');

        person.words_count.sort(function (a, b) {
            return b[1] - a[1];
        });
        let total_number_of_words = 0;
        Object.keys(person.words_count).forEach((index) => {
            if (person.words_count[index] !== undefined) {
                let word_div = $(document.createElement("div"))
                    .addClass('word')
                    .text(' ' + person.words_count[index][0] + ' ' + person.words_count[index][1]);
                person_words_div.prepend(word_div);
                total_number_of_words += person.words_count[index][1];
            }

        });
        let total_number_of_words_div = $(document.createElement("div"))
            .addClass('total-number')
            .text(total_number_of_words);

        person_words_div.prepend(total_number_of_words_div);

        console.log(total_number_of_words);
        return person_words_div;
    }

    createPersonDiv(person) {
        return $(document.createElement("div"))
            .addClass('person' + person.index)
            .addClass('person');
    }

    createTitle(person) {
        return $(document.createElement("div"))
            .addClass('title')
            .text(person.name);
    }

    createSimpleBlock(person) {

        let person_messages_div_raw_text = $(document.createElement("div")).addClass("raw");
        person_messages_div_raw_text.text(person.raw_text);


        return person_messages_div_raw_text;
    }

}